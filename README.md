# SCHOOL MANAGEMENT SYSTEM 🏫

### Author: Debrath Sharma

---

![GitHub followers](https://img.shields.io/github/followers/madara-coder?style=social) ![GitHub User's stars](https://img.shields.io/github/stars/madara-coder?style=social)

## Technologies Used:

1. HTML5
2. CSS3
3. JS
4. PHP
5. MySql
